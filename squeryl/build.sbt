organization := "org.scalakoans"

name := "scala-koans-squeryl" 

version := "1.0.0"

scalaVersion := "2.9.1"

libraryDependencies ++= Seq("junit" % "junit" % "4.8" % "test", 
                            "org.scalatest" %% "scalatest" % "1.8" % "test",
                            "org.scalakoans" %% "scala-koans-runtime" % "1.0.1" % "test",
                            "org.squeryl" %% "squeryl" % "0.9.5" % "test",
                            "com.h2database" % "h2" % "1.2.127" % "test"
                           )
