package org.scalakoans.squeryl

import org.functionalkoans.forscala.support._


import org.squeryl._
import org.squeryl.dsl._;
import org.squeryl.PrimitiveTypeMode._;

object L4ManyToManyStatefulScope {

case class Person(val id: Long, val firstName:String, val secondName: String) 
                                                 extends KeyedEntity[Long]
{

  lazy val books = DBSchema.personsToBooks.leftStateful(this);

}

case class Book(val id: Long, val title: String) 
                                                 extends KeyedEntity[Long]
{

  lazy val relatedPersons = DBSchema.personsToBooks.rightStateful(this);

  // note - filter in memory, so be carefull
  def authors = relatedPersons.associations.filter(
                                           _.relation==BookRelation.AUTHOR)

}

object BookRelation extends Enumeration
{
  val AUTHOR = Value(1,"Author")
  val PERSONAGE = Value(2,"Personage")
}

case class PersonBook(val personId: Long, 
                      val bookId: Long,
                      relation: BookRelation.Value, // enum represented in db as id-s.
                      comments: Option[String] = None)
                              extends KeyedEntity[CompositeKey2[Long,Long]]
{

  def id = CompositeKey2(personId, bookId);

  // - hint for mapper.
  def this() = this(-1L, -1L, BookRelation.AUTHOR, Some(""));
}


object DBSchema extends Schema
{
  val persons = table[Person]
  val books = table[Book]
  val personsToBooks = manyToManyRelation(persons,books).via[PersonBook](
                     (p,b,a) => (a.personId === p.id, a.bookId === b.id)
                   )
}

};

class L4AboutManyToManyStatefulRelation extends KoanSuite
{

  import L4ManyToManyStatefulScope._

  koan("Stateful relations are many to many relations with additional data") {
     SquerylSupport.withTransaction("t4") {

         //let's create tables.
         DBSchema.create;

         import DBSchema._

         val karlMarks = persons insert Person(0,"Karl","Marks");
         val fridrihEngels = persons insert Person(0,"Fridrix","Engels");
         val piterPen = persons insert Person(0,"Piter","Pen");
         val jamesBarry = persons insert Person(0,"James","Barry");

         val kapital = books insert Book(0L,"Kapital");
         val piterPenBook = books insert Book(0L,"Piter Pen");
          
         personsToBooks insert PersonBook(personId = karlMarks.id, bookId = kapital.id, relation = BookRelation.AUTHOR )
         personsToBooks insert PersonBook(personId = fridrihEngels.id, bookId = kapital.id, relation = BookRelation.AUTHOR)

         personsToBooks insert PersonBook(personId = piterPen.id, bookId = piterPenBook.id, relation = BookRelation.PERSONAGE)
         personsToBooks insert PersonBook(personId = jamesBarry.id, bookId = piterPenBook.id, relation = BookRelation.AUTHOR)

         kapital.authors should have size(__)

         piterPenBook.authors should have size(__)
         piterPenBook.relatedPersons should have size(__)


         DBSchema.drop;
     }
     
  }
       

}

// vim: set ts=4 sw=4 et:
