package org.scalakoans.squeryl

import org.functionalkoans.forscala.support._


import org.squeryl._
import org.squeryl.dsl._;
import org.squeryl.PrimitiveTypeMode._;
import java.sql.Timestamp._;

object L2OneToManyScope {

case class Person(val id: Long, val firstName:String, val lastName: String,
                  val birthPlaceId: Long
                  ) 
                                                 extends KeyedEntity[Long]
{

  def birthPlace: ManyToOne[City] = DBSchema.birthPlaces.right(this);

}

case class City(val id: Long, val name: String, val countryId: Long) extends KeyedEntity[Long]
{
  def bornHere: OneToMany[Person] = DBSchema.birthPlaces.left(this);

  def country: ManyToOne[Country] = DBSchema.countryToCities.right(this);

}

case class Country(val id: Long, val title: String) extends KeyedEntity[Long]
{
  def cities: OneToMany[City] = DBSchema.countryToCities.left(this);
}

object DBSchema extends Schema
{
  val persons = table[Person]
  val cities = table[City]
  val countries = table[Country]

  val birthPlaces = oneToManyRelation(cities,persons).via(
                       (c,p) => c.id === p.birthPlaceId
                    );

  val countryToCities = oneToManyRelation(countries, cities).via(
                          (cn,ct) => cn.id === ct.countryId
                        )

  // add 'capital city' relations

}

};

class L2AboutOneToManyRelation extends KoanSuite
{

  import L2OneToManyScope._

  koan("""One to many relations""") {
     SquerylSupport.withTransaction("t1") {

         //let's create tables.
         DBSchema.create;

         import DBSchema._

         val ukraine = countries insert Country(1L,"Ukraine")
         val germany = countries insert Country(2L,"Germany")
         val poland = countries insert Country(3L,"Poland")

         val kiev = cities insert City(-1L,"Kiev",ukraine.id);
         val lviv = cities insert City(-1L,"Lviv",ukraine.id);
         val berlin = cities insert City(-1L,"Berlin",germany.id);
         val munhen = cities insert City(-1L,"Munhen",germany.id);
         val trir = cities insert City(-1L,"Trir",germany.id);
         val wuppertal = cities insert City(-1L,"Wuppertal",germany.id);
         val varshava = cities insert City(-1L,"Varshava",poland.id);
         val krakov = cities insert City(-1L,"Krakov",poland.id);

         val p1 = persons insert Person(0,"Karl","Marks",trir.id);
         val p2 = persons insert Person(0,"Fridrix","Engels",wuppertal.id);
         val p3 = persons insert Person(0,"Stanislav","Lem",lviv.id);

         val fromUkraine = from(persons, cities, countries)(
                                  (p,ct,cn) => where(ct.id === p.birthPlaceId
                                                   and
                                                     cn.id === ct.countryId
                                                   and
                                                     cn.id === ukraine.id
                                                   )
                                             select(p))

         fromUkraine should have size(__)

         val lem = from(fromUkraine)(p => where(p.lastName==="Lem")
                                           select(p)).single

         lem.lastName should be (__)

         ukraine.cities should have size(__) 


         DBSchema.drop;
     }
     
  }
       

}

// vim: set ts=4 sw=4 et:
