organization := "org.scalakoans"

name := "Scala Koans Exercises (base)" 

version := "1.0.1"

scalaVersion := "2.9.1"

libraryDependencies ++= Seq("junit" % "junit" % "4.8" % "test", 
                            "org.scalatest" %% "scalatest" % "1.8" % "test",
                            "org.scalakoans" %% "scala-koans-runtime" % "1.0.1" % "test"
                           )
