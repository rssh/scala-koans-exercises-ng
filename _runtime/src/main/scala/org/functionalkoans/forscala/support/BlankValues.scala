package org.functionalkoans.forscala.support

import org.scalatest._
import org.scalatest.matchers._

trait BlankValues 
          extends ShouldMatchers
{
    class ReplaceWithCorrectException extends Exception


    //val __ = "Should be filled in"
 
    def  __ : __ = {
        throw new TestPendingException
    }

    implicit def matcherToString(m: Matcher[Any]):String =
        throw new TestPendingException

    implicit def matcherToInt(m: Matcher[Any]):Int =
        throw new TestPendingException


    class ___ extends ReplaceWithCorrectException {
        override def toString() = "___"
    }

    class ReplaceWithCorrectType

    class __ extends Matcher[Any] {
        override def toString() = "__"
        override def apply(a:Any): MatchResult =
            throw new TestPendingException
    }
}

