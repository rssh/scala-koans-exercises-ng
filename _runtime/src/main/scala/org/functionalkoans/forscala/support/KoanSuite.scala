package org.functionalkoans.forscala.support
import org.scalatest._
import org.scalatest.events._
import org.scalatest.matchers._

trait KoanSuite extends FunSuite with SeveredStackTraces 
                                 with BlankValues
                                 with ShouldMatchers
{

    override def runTests(testName : Option[String], reporter : Reporter, stopper : Stopper, filter : Filter,
                         configMap : Map[String, Any], distributor : Option[Distributor], tracker : Tracker) {       


      if (!Master.studentNeedsToMeditate) {

       if (testName == null)
          throw new NullPointerException("testName was null")
        if (reporter == null)
          throw new NullPointerException("reporter was null")
        if (stopper == null)
          throw new NullPointerException("stopper was null")
        if (filter == null)
          throw new NullPointerException("filter was null")
        if (configMap == null)
          throw new NullPointerException("configMap was null")
        if (distributor == null)
          throw new NullPointerException("distributor was null")
        if (tracker == null)
          throw new NullPointerException("tracker was null")

        class KoanReporter(wrappedReporter : Reporter) extends Reporter {

          var failed = false;

          def failure(event: Event) {
             failed = true;
             info("*****************************************")
             info("*****************************************")
             info("")
             info("")
             info("")
             info(Master.studentFailed(event))
             info("")
             info("")
             info("")
             info("*****************************************")
             info("*****************************************")
          }



            override def apply(event : Event) = {
                event match {
                    case e: TestIgnored => failure(event)
                    case e: TestFailed => failure(event)
                    case e: TestPending => failure(event)
                    case _ =>
                }
                wrappedReporter(event)
            }
        }

        val stopRequested = stopper

        // If a testName is passed to run, just run that, else run the tests returned
        // by testNames.
        testName match {
          case Some(tn) => runTest(tn, reporter, stopRequested, configMap, tracker)
          case None =>
            var failed = false
            for (test <- testNames.iterator) {
              if (failed == false) {
                  val koanReporter = new KoanReporter(reporter)
                  runTest(test, koanReporter, stopper, configMap, tracker)
                  failed = koanReporter.failed;
              }
            }
        }

      }
    }

    def koan(name : String)(fun: => Unit) = test(name)(fun)

    def meditate() = pending


}
